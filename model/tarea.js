// Módulo Interno

const mongoose = require("mongoose")

// Esquema Tarea

const esquemaTarea = new mongoose.Schema({

    idUsuario: String,
    nombre: String,
    descripcion: String,
    estado: String,
    fecha: {
        type: Date,
        default: Date.now
    },
})

// Exports

const Tarea = mongoose.model("tarea", esquemaTarea)

module.exports.Tarea = Tarea