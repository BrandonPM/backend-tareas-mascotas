// Módulos Internos

const express = require("express")
const router = express.Router()

// Módulos Propios

const { Usuario } = require ("../model/usuario")

// Rutas

router.post("/", async(req, res) => {

    // Si el usuario existe...
    let usuario = await Usuario.findOne({ correo: req.body.correo})

    
    // Si el usuario existe en la BD
    if (usuario.correo == usuario) return res.status(400).send("El correo ya existe en la BD")

    if (usuario) return res.status(400).send("El usuario existe en la BD")

    // Si el usuario no existe
    usuario = new Usuario({
        nombre: req.body.nombre,
        correo: req.body.correo,
        password: req.body.password
    })

    // Guardar el usuario en la BD y generar el JWT 
    const result = await usuario.save()
    const jwtToken = usuario.generateJWT()
    res.status(200).send({jwtToken})
})

// Módulos Export

module.exports = router