// Módulos Internos

const express = require("express")
const router = express.Router()

// Módulos Propios

const { Usuario } = require ("../model/usuario")

// Rutas

router.post("/", async(req, res) => {

    // Si el usuario existe...
    let usuario = await Usuario.findOne({ correo: req.body.correo})

    // Si el usuario existe en la BD
    if (!usuario) return res.status(400).send("Correo o contraseña incorrectos")

    // Si existe el usuario
    if (usuario.password != req.body.password) return res.status(400).send("Correo o contraseña incorrectos")

    // Si pasa generar el JWT
    const jwtToken = usuario.generateJWT()
    res.status(200).send({jwtToken})
})

// Exports
module.exports = router