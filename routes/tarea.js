// Módulos Internos

const express = require("express")
const router = express.Router()

// Módulos Propios

const { Usuario } = require ("../model/usuario")
const { Tarea } = require ("../model/tarea")

const auth = require("../middleware/auth")

// Rutas

router.post ("/", auth, async(req, res) => {

    // Tomar el Usuario
    const usuario = await Usuario.findById(req.usuario._id)

    // Si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe")

    const tarea = new Tarea( {
        idUsuario: usuario._id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        estado: req.body.estado,
    })

    const result = await tarea.save()   
    res.status(200).send(result)
})

// Listar tareas del usuario registrado

router.get("/lista", auth, async(req, res) => {

    // Tomar el ususario loggeado
    const usuario = await Usuario.findById(req.usuario._id)

    // Si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe en la BD")

    // Si el usuario existe, mostrar todas sus tareas
    const tarea = await Tarea.find({idUsuario: req.usuario._id})

    res.send(tarea)
})

// Editar una tarea de un usuario registrado

router.put("/", auth, async(req, res) => {

    // Tomar el ususario loggeado
    const usuario = await Usuario.findById(req.usuario._id)

    // Si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe en la BD")

    // Si el usuario existe
    const tarea = await Tarea.findByIdAndUpdate(
        req.body._id,
        {
            idUsuario: usuario._id,
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            estado: req.body.estado
        },
        {
            new: true
        })

    if (!tarea) return res.status(400).send("El usuario no tiene tareas")

    // Si existen tareas
    res.status(200).send(tarea)
})

// Eliminar una tarea del usuario loggeado

router.delete("/:_id", auth, async(req, res) => {

    // Tomar el ususario loggeado
    const usuario = await Usuario.findById(req.usuario._id)

    // Si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe en la BD")

    const tarea = await Tarea.findByIdAndDelete(req.params._id)

    // Si no existe la tarea
    if (!tarea) return res.status(400).send("El usuario no tiene tareas")

    // Si existe la tarea
    res.status(200).send({ message: "Tarea Eliminada" })
})

// Exports

module.exports = router