// Módulos Internos

const express = require("express")
const router = express.Router()

// Módulos Propios

const { Usuario } = require ("../model/usuario")
const { Mascota } = require ("../model/mascotas")

const auth = require("../middleware/auth")

// Rutas

router.post ("/", auth, async(req, res) => {

    // Tomar el Usuario
    const usuario = await Usuario.findById(req.usuario._id)

    // Si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe")

    const mascota = new Mascota( {
        idUsuario: usuario._id,
        nombre: req.body.nombre,
        tipo: req.body.tipo,
        descripcion: req.body.descripcion,
    })

    if (mascota == mascota) return res.status(400).send("La mascota ya existe")

    const result = await mascota.save()   
    res.status(200).send(result)
})

// Listar mascotas del usuario registrado

router.get("/lista", auth, async(req, res) => {

    // Tomar el ususario loggeado
    const usuario = await Usuario.findById(req.usuario._id)

    // Si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe en la BD")

    // Si el usuario existe, mostrar todas sus tareas
    const mascota = await Mascota.find({idUsuario: req.usuario._id})

    res.send(mascota)
})

// Editar una mascota de un usuario registrado

router.put("/", auth, async(req, res) => {

    // Tomar el ususario loggeado
    const usuario = await Usuario.findById(req.usuario._id)

    // Si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe en la BD")

    // Si el usuario existe
    const mascota = await Mascota.findByIdAndUpdate(
        req.body._id,
        {
            idUsuario: usuario._id,
            nombre: req.body.nombre,
            tipo: req.body.tipo,
            descripcion: req.body.descripcion
        },
        {
            new: true
        })

    if (!mascota) return res.status(400).send("El usuario no tiene mascotas")

    // Si existen tareas
    res.status(200).send(mascota)
})

// Eliminar una mascota del usuario loggeado

router.delete("/:_id", auth, async(req, res) => {

    // Tomar el ususario loggeado
    const usuario = await Usuario.findById(req.usuario._id)

    // Si el usuario no existe
    if (!usuario) return res.status(400).send("El usuario no existe en la BD")

    const mascota = await Mascota.findByIdAndDelete(req.params._id)

    // Si no existe la mascota
    if (!mascota) return res.status(400).send("El usuario no tiene mascotas")

    // Si existe la mascota
    res.status(200).send({ message: "Mascota Eliminada" })
})

// Exports

module.exports = router