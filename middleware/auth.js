// Módulos Internos

const jwt = require("jsonwebtoken")

// Crear Función de Middleware

function auth (req, res, next) {

    let jwtToken = req.header("Authorization")

    jwtToken = jwtToken.split(" ")[1]

    // Validar si hay un Token

    if (!jwtToken) return res.status(400).send("No existe un Token para validar")

    // Si existe un Token
    try {
        const payload = jwt.verify(jwtToken, "clave")
        req.usuario = payload
        next()
    } catch (error) {
        res.status(400).send("Token no válido, sin autorización")
    }
} 

// Exports

module.exports = auth