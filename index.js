// Módulos Internos

const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")

// Módulos Creados

const usuario = require("./routes/usuario")
const auth = require("./routes/auth")
const tarea = require("./routes/tarea")
const mascota = require("./routes/mascotas")

// APP

const app = express()
app.use(express.json())

// Routas

app.use("/api/usuario", usuario)
app.use("/api/auth", auth)
app.use("/api/tarea", tarea)
app.use("/api/mascotas", mascota)

// Puerto de Ejecución

const port = process.env.PORT || 3001
app.listen(port, () => console.log("... Escuchando el puerto: " + port))

mongoose.connect("mongodb://localhost/tareasbd344", {
    useNewUrlParser: true,
    userFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true
})

.then(() => console.log("Conexión a MongoDB - OK"))
.catch((error) => console.log("Conexión a MongoDB OFF " + error))